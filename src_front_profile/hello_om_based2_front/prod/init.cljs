(ns hello-om-based2-front.init
    (:require [hello-om-based2-front.core :as core]
              [hello-om-based2-front.conf :as conf]))

(enable-console-print!)

(defn start-descjop! []
  (core/init! conf/setting))

(start-descjop!)
